## aperorama #1 _ 11.07.17 _ La Paillasse

### Des aperorama pour défricher le terrain.
_miné ?_
Salut je modifie!!~~~~~~~~~~~~
La perspective de création d’un nouveau site communautaire enflamme rapidement les esprits qui s’intéressent un tant soit peu au sujet. Beaucoup s’y sont déjà essayé, dans la dynamique du « web 2.0 » et plusieurs acteurs de ces expérimentations nous ont déjà mis en garde contre les nombreux écueils qu’ils ont pu eux-mêmes rencontrer, ou auxquels se sont heurtés des projets dont ils étaient témoins. Tout en nous affirmant, à chaque fois, que le chantier orama était nécessaire, absolument. Une utopie à rendre concrète.

Ce premier aperorama était de ces moments importants, de confrontation bienveillante mais sans concession (aucune) entre les visées du projet et l’état et les orientations de son développement.
Il en ressort notamment que, toute nécessaire et passionnante que soit la proposition, elle n’apporterait pas encore dans sa forme actuelle un intérêt suffisant en terme d’utilité ou de commodité d’utilisation pour les utilisateurs, seuls critères garants d’une pérennité de la plateforme et de sa communauté.
L’idée étant que, si les partis-pris de liberté et d’ouverture qui sous-tendent la conception du site peuvent bel et bien recevoir le soutien sans faille de nombre de personnes, ils ne suffiront jamais à transformer les usages de ceux que nous voulons toucher. Avec le site orama.ninja, il s’agit de créer de nouvelles habitudes pour nos utilisateurs en travaillant sur le service rendu et l’ergonomie. chantier gigantesque. Et peut-être une vision encore trop imprécise ?
“Le web est mort” entend-on ce soir-là.
La création d’un (énième) site web est-elle pertinente sous la forme que nous envisageons ?
-> Nous pensons que le site ne s’imagine pas indépendamment des causeries, qui le nourrissent, et qui s’en nourrissent. orama.ninja n’est pas une fin en soi.
Nous sommes aussi conscients qu’il est nécessaire d’imaginer orama.ninja comme un objet culturel/artistique à part entière, d’une part, et d’autre part de permettre à son contenu d’être édité, enrichi, consulté de manière multiple et variée. Ainsi, la mise en place de flux RSS et la création d’une API sont des sous-chantiers primordiaux, pour permettre au projet de se connecter aux usages.

La mise en place de newsletters régulières, et autres moyens à développer, inventer pour maintenir l’attrait de la plateforme auprès de ses utilisateurs est également évoquée pendant la soirée.

On soulève aussi la question du positionnement du projet vis-à-vis des institutions et notamment du Ministère de la Culture qui soutient le développement d’orama à travers Matrice. L’anonymat qui prime dans certains réseaux alternatifs (cf. radar.squat.net ou utopies-concretes.org) est-il la voie à suivre ?
